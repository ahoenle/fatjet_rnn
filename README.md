# fatjet RNN
#### A direct large-$`R`$ jet tagger

> I'm not fat, I'm just big-coned - _Every fat jet ever_

### Python requirement

This repository runs with `python >= 3.6`.

### Purpose of this README

I know that this README should provide a fancy documentation over all code.
But so far this is mainly an experiment and everything is changing rapidly.
I will try to provide a more sophisticated documentation once persistent modules are scraped out.

### Data to download

**Note:** There are two combinations of tags available

- MC16a: `e3820_s3126_r9364_r9315_p3600`
- MC16d: `e5984_s3126_r10201_r10210_p3600`

These are the datasets we use for training and evaluating the ML model.

- Higgs:
    - DSID 301504, **TODO: Check mass** (want lower mass point)
        - `rucio get user.dguest.301504.hbbTraining.e3820_s3126_s3136_r9364_r9315_p3600.v1_output.h5`
    - DSID 301505, $`m_{G*} = 2.5\,\mathrm{TeV}`$
        - `rucio get user.dguest.301505.hbbTraining.e3820_s3126_r9364_r9315_p3600.v1_output.h5`
    - DSID 301506, **TODO: Check mass** (want higher mass point)
        - `rucio get user.dguest.301506.hbbTraining.e3820_s3126_s3136_r9364_r9315_p3600.v1_output.h5`
- Top:
    - DSID 301331, $`m_{Z'} = 2.5\,\mathrm{TeV}`$
        - `rucio get --nrandom 1 user.dguest.301331.hbbTraining.e4061_s3126_r9364_r9315_p3600.v1_output.h5`
    - DSID 301333 $`m_{Z'} = 3.0\,\mathrm{TeV}`$
        - `rucio get --nrandom 1 user.dguest.301333.hbbTraining.e3723_s3126_r9364_r9315_p3600.v1_output.h5`
- QCD:
    - DSID 361025, JZ5W
        - `rucio get --nrandom 1 user.dguest.361025.hbbTraining.e3668_s3126_r9364_r9315_p3600.v1_output.h5`
    - DSID 361026, JZ6W
        - `rucio get --nrandom 1 user.dguest.361026.hbbTraining.e3569_s3126_r9364_r9315_p3600.v1_output.h5`
- TODO: `fat jet pT < 2 TeV`

- TODO: Track eta > or < than fat jet eta (feature engineering)
#!/usr/bin/env python

'''
This script tests a model trained with keras.
For portability I run this in a docker image: ahoenle/tensorflow-notebook-with-packages

The Docker images has python 3.6.5 installed
'''

import os
os.environ['KERAS_BACKEND'] = 'tensorflow'

import argparse
import sys
import shutil
import json
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from sklearn.preprocessing import OneHotEncoder

from keras.models import load_model as lm
from keras import backend as K
K.set_learning_phase(0)

import numpy as np
np.random.seed(1989)
import tensorflow as tf
tf.set_random_seed(2016)

from utils import get_project_dir

sys.path.append(os.path.join(get_project_dir(), 'data_processing'))
# sys.path.append('/fatjet_rnn/data_processing')
from dataset import Dataset
from normalization import Normalizer
from jetattributes import trk_attr
from truthcategories import *

MASKING_VALUE = -999

color_higgs = '#A11E22'
color_top = '#E8A631'
color_qcd = '#276478'
color_other = '#57575F'


def load_data(cfg):
    higgs_name = os.path.join(get_project_dir(), cfg['higgs']['test_data'])
    top_name = os.path.join(get_project_dir(), cfg['top']['test_data'])
    qcd_name = os.path.join(get_project_dir(), cfg['qcd']['test_data'])

    higgs = Dataset(higgs_name, max_read=10000, truth_cut=CAT_HIGGS)
    top = Dataset(top_name, max_read=10000, truth_cut=CAT_TOP)
    qcd = Dataset(qcd_name, max_read=10000, truth_cut=CAT_QCD)

    normalizer = Normalizer()
    normalizer.load_from(os.path.join(get_project_dir(), cfg['normalization']))
    higgs.values = normalizer.normalize(higgs)
    top.values = normalizer.normalize(top)
    qcd.values = normalizer.normalize(qcd)

    tracks = np.concatenate((higgs.values, top.values, qcd.values), axis=0)
    weights = np.concatenate((higgs.weights, top.weights, qcd.weights), axis=0)

    # all variables that go into the training
    variables = [
        'numberOfInnermostPixelLayerHits',
        'numberOfNextToInnermostPixelLayerHits',
        'numberOfPixelHits',
        'numberOfPixelHoles',
        'numberOfPixelSharedHits',
        'numberOfPixelSplitHits',
        'numberOfSCTHits',
        'numberOfSCTHoles',
        'numberOfSCTSharedHits',
        'IP3D_signed_d0',
        'IP3D_signed_z0',
        'IP3D_signed_d0_significance',
        'IP3D_signed_z0_significance',
        'dr_fj',      # added by us
        'ptfrac_fj',  # added by us
        'min_deta',   # added by us
        'max_deta'    # added by us
    ]

    indices = [trk_attr[v] for v in variables]
    X = tracks[:, :, indices]
    Y = tracks[:, :, trk_attr['truth_category']]

    nan_indices = np.where(np.isnan(X))
    X[nan_indices] = MASKING_VALUE

    return X, Y, weights


def load_model(cfg):
    # model = lm(os.path.join(get_project_dir(), cfg['model']))
    checkpoint_dir = os.path.join(
        get_project_dir(), 'learning', 'models', cfg['name'])
    checkpoint_file = None
    min_loss = 100
    for cp in sorted(os.listdir(checkpoint_dir)):
        try:
            loss = float(cp[cp.find('vl')+2:].replace('.h5', ''))
        except:
            print(
                f'load_model: Warning: Ignoring file with unexpected format: {cp}')
            continue
        if loss < min_loss:
            checkpoint = os.path.join(checkpoint_dir, cp)
            min_loss = loss
    print(f'Loading model with loss of {min_loss} from file {checkpoint}')

    model = lm(checkpoint)
    print(model.summary())

    return model


def plot_sb_distributions(predictions, Y, weights, plotdir, name):
    '''
    Plot the distributions of signal and background
    Because we have three predictions we make three plots
      1) higgs vs. not higgs
      2) top vs. not top
      3) qcd vs. not qcd
    The categories are determined from the truth label stored in Y
    '''
    fig, ax = plt.subplots(1, 3, sharey=True)
    fig.set_figheight(5)
    fig.set_figwidth(13)

    nbins = 50
    weights /= nbins  # to account for bin width in plot

    higgs = [predictions[i][0]
             for i in range(len(predictions)) if Y[i][0] == 1]
    not_higgs = [predictions[i][0]
                 for i in range(len(predictions)) if Y[i][0] == 0]
    higgs_weights = [weights[i][0]
                     for i in range(len(weights)) if Y[i][0] == 1]
    not_higgs_weights = [weights[i][0]
                         for i in range(len(weights)) if Y[i][0] == 0]
    do_density = True
    ax[0].hist(not_higgs, weights=not_higgs_weights, density=do_density, bins=nbins, color=color_other,
               alpha=.5, histtype='stepfilled', label='not higgs')
    ax[0].hist(higgs, weights=higgs_weights, density=do_density, bins=nbins, color=color_higgs,
               alpha=.7, histtype='step', label='higgs', linewidth=2)
    ax[0].set_xlabel('RNN Higgs prediction')
    ax[0].set_ylabel('arbitrary units')
    handles, labels = ax[0].get_legend_handles_labels()
    ax[0].legend(handles[::-1], labels[::-1], loc='upper center')

    top = [predictions[i][1]
           for i in range(len(predictions)) if Y[i][1] == 1]
    not_top = [predictions[i][1]
               for i in range(len(predictions)) if Y[i][1] == 0]
    top_weights = [weights[i][0]
                   for i in range(len(weights)) if Y[i][1] == 1]
    not_top_weights = [weights[i][0]
                       for i in range(len(weights)) if Y[i][1] == 0]
    ax[1].hist(not_top, weights=not_top_weights, density=do_density, bins=nbins, color=color_other,
               alpha=.5, histtype='stepfilled', label='not top')
    ax[1].hist(top, weights=top_weights, density=do_density, bins=nbins, color=color_top,
               alpha=.7, histtype='step', label='top', linewidth=2)
    ax[1].set_xlabel('RNN Top prediction')
    handles, labels = ax[1].get_legend_handles_labels()
    ax[1].legend(handles[::-1], labels[::-1], loc='upper center')

    qcd = [predictions[i][2]
           for i in range(len(predictions)) if Y[i][2] == 1]
    not_qcd = [predictions[i][2]
               for i in range(len(predictions)) if Y[i][2] == 0]
    qcd_weights = [weights[i][0]
                   for i in range(len(weights)) if Y[i][2] == 1]
    not_qcd_weights = [weights[i][0]
                       for i in range(len(weights)) if Y[i][2] == 0]
    ax[2].hist(not_qcd, weights=not_qcd_weights, density=do_density, bins=nbins, color=color_other,
               alpha=.5, histtype='stepfilled', label='not qcd')
    ax[2].hist(qcd, weights=qcd_weights, density=do_density, bins=nbins, color=color_qcd,
               alpha=.7, histtype='step', label='qcd', linewidth=2)
    ax[2].set_xlabel('RNN QCD prediction')
    handles, labels = ax[2].get_legend_handles_labels()
    ax[2].legend(handles[::-1], labels[::-1], loc='upper center')

    plt.savefig(os.path.join(plotdir, f'{name}.png'), dpi=180)
    plt.savefig(os.path.join(plotdir, f'{name}.pdf'))
    return fig, ax


def plot_rocs(predictions, Y, weights, plotdir, name):
    '''
    Plot the ROC curves for higgs efficiency vs. top rej and higgs efficiency vs. qcd rej
    '''
    stepsize = 1e-2
    cut = 1.0
    higgs_eff = []
    top_rej = []  # rejection is defined as 1 - efficiency
    qcd_rej = []
    top_sup = []  # suppression is defined as 1 / efficiency
    qcd_sup = []

    higgs_predictions = [predictions[i][0]
                         for i in range(len(predictions)) if Y[i][0] == 1]
    top_predictions = [predictions[i][1]
                       for i in range(len(predictions)) if Y[i][1] == 1]
    qcd_predictions = [predictions[i][2]
                       for i in range(len(predictions)) if Y[i][2] == 1]

    while cut > 0:
        heff = sum([1.0 for p in higgs_predictions if p >= cut]) / \
            float(len(higgs_predictions))
        teff = sum([1.0 for p in top_predictions if p >= cut]) / \
            float(len(top_predictions))
        qeff = sum([1.0 for p in qcd_predictions if p >= cut]) / \
            float(len(qcd_predictions))

        higgs_eff.append(heff)
        top_rej.append(1. - teff)
        qcd_rej.append(1. - qeff)
        try:
            top_sup.append(1. / teff)
        except ZeroDivisionError:
            top_sup.append(np.nan)
        try:
            qcd_sup.append(1. / qeff)
        except ZeroDivisionError:
            qcd_sup.append(np.nan)

        cut -= 0.01

    fig, ax = plt.subplots(2, 2, sharex=True, sharey='row')
    fig.set_figheight(10)
    fig.set_figwidth(13)

    # need to convert to numpy array for NaN masking
    higgs_eff = np.array(higgs_eff)
    top_sup = np.array(top_sup)
    qcd_sup = np.array(qcd_sup)

    ax[0, 0].plot(higgs_eff, top_rej, 'o-', color=color_top, markeredgewidth=0)
    ax[0, 1].plot(higgs_eff, qcd_rej, 'o-', color=color_qcd, markeredgewidth=0)
    ax[1, 0].plot(higgs_eff[~np.isnan(top_sup)], top_sup[~np.isnan(top_sup)],
                  'o-', color=color_top, markeredgewidth=0)
    ax[1, 1].plot(higgs_eff[~np.isnan(qcd_sup)], qcd_sup[~np.isnan(qcd_sup)],
                  'o-', color=color_qcd, markeredgewidth=0)

    ax[0, 0].set_xlabel('Higgs efficiency')
    ax[0, 1].set_xlabel('Higgs efficiency')
    ax[1, 0].set_xlabel('Higgs efficiency')
    ax[1, 1].set_xlabel('Higgs efficiency')

    ax[0, 0].set_ylabel('Top rejection (1 - efficiency)')
    ax[0, 1].set_ylabel('QCD rejection (1 - efficiency)')
    ax[1, 0].set_ylabel('Top suppression (1 / efficiency)')
    ax[1, 1].set_ylabel('QCD suppression (1 / efficiency)')

    fig.savefig(os.path.join(plotdir, f'{name}.png'), dpi=180)
    fig.savefig(os.path.join(plotdir, f'{name}.pdf'))


def likelihood(x, f_t=0.5):
    """
    Calculate likelihood

    Args:
       x: An event x of shape (1, 3) with 3 predictions: p_higgs, p_top, p_qcd
       f_t: Parameter - fraction of top bg in the sample

    Returns:
       The likelihood calculated from the three predictions

    TODO: Improve description, add formula
    """
    p_higgs = x[0]
    p_top = x[1]
    p_qcd = x[2]
    likely = p_higgs / ((f_t * p_top) + ((1 - f_t) * p_qcd))
    eps = 1e-10  # to deal with zero values
    return np.log(likely + eps)


def plot_likelihood(likelies, Y, weights, plotdir, name):
    """
    Make a s/b like plot for the likelihoods
    """

    higgs = [likelies[i]
             for i in range(len(predictions)) if Y[i][0] == 1]
    top = [likelies[i]
           for i in range(len(predictions)) if Y[i][1] == 1]
    qcd = [likelies[i]
           for i in range(len(predictions)) if Y[i][2] == 1]

    higgs_weights = [weights[i][0]
                     for i in range(len(predictions)) if Y[i][0] == 1]
    top_weights = [weights[i][0]
                   for i in range(len(predictions)) if Y[i][1] == 1]
    qcd_weights = [weights[i][0]
                   for i in range(len(predictions)) if Y[i][2] == 1]

    fig, ax = plt.subplots()
    fig.set_figheight(5)
    fig.set_figwidth(4.33)

    do_density = True
    ax.hist(qcd, weights=qcd_weights, density=do_density, bins=50, color=color_qcd,
            alpha=.5, histtype='stepfilled', label='qcd')
    ax.hist(top, weights=top_weights, density=do_density, bins=50, color=color_top,
            alpha=.5, histtype='stepfilled', label='top')
    ax.hist(higgs, weights=higgs_weights, density=do_density, bins=50, color=color_higgs,
            alpha=.7, histtype='step', label='higgs', linewidth=2)
    ax.set_xlabel(
        r'$D_{\mathrm{RNN}} = \ln \; \frac{p_h}{f_t p_t + (1 - f_t) p_{\mathrm{qcd}}}$')
    ax.set_ylabel('arbitrary units')
    handles, labels = ax.get_legend_handles_labels()
    # ax.legend(handles[::-1], labels[::-1], loc='upper right')
    ax.legend(handles[::-1], labels[::-1])

    ax.set_xlim(-8, 5)

    fig.savefig(os.path.join(plotdir, f'{name}.png'), dpi=180)
    fig.savefig(os.path.join(plotdir, f'{name}.pdf'))


def plot_likely_roc(likelies, Y, weights, plotdir, name):
    """
    Make ROC curve for the likelihoods
    """
    stepsize = 1e-2
    cut = np.max(likelies)
    higgs_eff = []
    top_rej = []  # rejection is defined as 1 - efficiency
    qcd_rej = []
    top_sup = []  # suppression is defined as 1 / efficiency
    qcd_sup = []  # suppression is defined as 1 / efficiency

    higgs = [likelies[i]
             for i in range(len(predictions)) if Y[i][0] == 1]
    top = [likelies[i]
           for i in range(len(predictions)) if Y[i][1] == 1]
    qcd = [likelies[i]
           for i in range(len(predictions)) if Y[i][2] == 1]

    higgs_weights = [weights[i][0]
                     for i in range(len(predictions)) if Y[i][0] == 1]
    top_weights = [weights[i][0]
                   for i in range(len(predictions)) if Y[i][1] == 1]
    qcd_weights = [weights[i][0]
                   for i in range(len(predictions)) if Y[i][2] == 1]

    sum_higgs = np.sum(higgs_weights)
    sum_top = np.sum(top_weights)
    sum_qcd = np.sum(qcd_weights)

    while cut > np.min(likelies):
        heff = sum([higgs_weights[i] for i in range(len(higgs))
                    if higgs[i] >= cut]) / sum_higgs
        teff = sum([top_weights[i] for i in range(len(top))
                    if top[i] >= cut]) / sum_top
        qeff = sum([qcd_weights[i] for i in range(len(qcd))
                    if qcd[i] >= cut]) / sum_qcd

        higgs_eff.append(heff)
        top_rej.append(1. - teff)
        qcd_rej.append(1. - qeff)
        try:
            top_sup.append(1. / teff)
        except ZeroDivisionError:
            top_sup.append(np.nan)
        try:
            qcd_sup.append(1. / qeff)
        except ZeroDivisionError:
            qcd_sup.append(np.nan)

        cut -= 0.1

    fig, ax = plt.subplots(2, 2, sharex=True, sharey='row')
    fig.set_figheight(10)
    fig.set_figwidth(13)

    # need to convert to numpy array for NaN masking
    higgs_eff = np.array(higgs_eff)
    top_sup = np.array(top_sup)
    qcd_sup = np.array(qcd_sup)

    ax[0, 0].plot(higgs_eff, top_rej, 'o-', color=color_top, markeredgewidth=0)
    ax[0, 1].plot(higgs_eff, qcd_rej, 'o-', color=color_qcd, markeredgewidth=0)
    ax[1, 0].plot(higgs_eff[~np.isnan(top_sup)], top_sup[~np.isnan(top_sup)],
                  'o-', color=color_top, markeredgewidth=0)
    ax[1, 1].plot(higgs_eff[~np.isnan(qcd_sup)], qcd_sup[~np.isnan(qcd_sup)],
                  'o-', color=color_qcd, markeredgewidth=0)

    ax[0, 0].set_xlabel('Higgs efficiency')
    ax[0, 1].set_xlabel('Higgs efficiency')
    ax[1, 0].set_xlabel('Higgs efficiency')
    ax[1, 1].set_xlabel('Higgs efficiency')

    ax[0, 0].set_ylabel('Top rejection (1 - efficiency)')
    ax[0, 1].set_ylabel('QCD rejection (1 - efficiency)')
    ax[1, 0].set_ylabel('Top suppression (1 / efficiency)')
    ax[1, 1].set_ylabel('QCD suppression (1 / efficiency)')

    fig.suptitle('Efficiencies determined via likelihood cut')

    fig.savefig(os.path.join(plotdir, f'{name}.png'), dpi=180)
    fig.savefig(os.path.join(plotdir, f'{name}.pdf'))


def one_hot_encode(X, Y):
    enc = OneHotEncoder()
    enc.fit(Y.reshape(-1, 1))
    ohe_Y = (
        enc.transform(Y.reshape(-1, 1))
        .toarray()
        .reshape(X.shape[0], X.shape[1], -1)
    )
    # at this point ohe_Y has shape (nevents, ntracks, ncategories)
    # but after the LSTM layer the ntracks dimension is gone
    # we need shape (nevents, ncategories)
    reshape_Y = np.empty((ohe_Y.shape[0], ohe_Y.shape[2]))
    reshape_Y[:, :] = ohe_Y[:, 0, :]
    return reshape_Y


def prep_plotdir(cfg):
    plotdir = os.path.join(get_project_dir(), 'plots',
                           'model_evaluation', cfg['name'])
    if os.path.isdir(plotdir):
        shutil.rmtree(plotdir)
    os.makedirs(plotdir)
    return plotdir


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Evaluate')
    parser.add_argument('cfg', help='Path to the cfg in json format.')
    args = parser.parse_args()

    with open(args.cfg) as cfgfile:
        cfg = json.load(cfgfile)
    model = load_model(cfg)

    X, Y, weights = load_data(cfg)
    Y = one_hot_encode(X, Y)

    perf = model.evaluate(X, Y, batch_size=32, sample_weight=weights[:, 0])
    print(f'Model performance on test data: {perf} ({model.metrics_names})')

    predictions = model.predict(X)

    plotdir = prep_plotdir(cfg)

    plot_sb_distributions(predictions, Y, weights, plotdir, name='sb')
    plot_rocs(predictions, Y, weights, plotdir, name='rocs')
    # calculate likelihoods
    likelies = np.apply_along_axis(likelihood, 1, predictions, f_t=0.5)
    plot_likelihood(likelies, Y, weights, plotdir, name='likelihoods')
    plot_likely_roc(likelies, Y, weights, plotdir, name='likely_rocs')

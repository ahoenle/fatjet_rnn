#!/bin/bash

setupATLAS
lsetup rucio
voms-proxy-init --voms atlas

# m_G* = 2.5 TeV higgs sample
rucio get --nrandom 4 user.dguest.301505.hbbTraining.e3820_s3126_r9364_r9315_p3600.p3_output.h5
# m_Z' = 2.5 TeV top sample
rucio get --nrandom 4 user.dguest.301331.hbbTraining.e4061_s3126_r9364_r9315_p3600.p3_output.h5
# JZ5W qcd sample
rucio get --nrandom 4 user.dguest.361025.hbbTraining.e3668_s3126_r9364_r9315_p3600.p3_output.h5

mkdir data >&/dev/null
cd data
mkdir higgs top qcd >&/dev/null

mv ../user.dguest.301505.hbbTraining.e3820_s3126_r9364_r9315_p3600.p3_output.h5 higgs
mv ../user.dguest.301331.hbbTraining.e4061_s3126_r9364_r9315_p3600.p3_output.h5 top
mv ../user.dguest.361025.hbbTraining.e3668_s3126_r9364_r9315_p3600.p3_output.h5 qcd

cd ..

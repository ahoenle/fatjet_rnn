#!/usr/bin/env python

'''
This script tests a model trained with keras.
For portability I run this in a docker image: ahoenle/tensorflow-notebook-with-packages

The Docker images has python 3.6.5 installed
'''

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

from keras.models import load_model as lm

# os.chdir('/fatjet_rnn')  # mountpoint of this script in the Docker image

def load_data():
    test_X_categorylike = np.load('data/test_X_categorylike.npy')
    test_X_highlevel = np.load('data/test_X_highlevel.npy')
    test_Y = np.load('data/test_y_highlevel.npy')
    return test_X_categorylike, test_X_highlevel, test_Y

def load_model():
    model = lm('models/2018-08-30-model.h5')
    model.load_weights('models/test.out')
    return model

def plot_sb_distributions(predictions, test_Y):
    '''
    Plot the distributions of signal and background
    Because we have three predictions we make three plots
      1) higgs vs. not higgs
      2) top vs. not top
      3) qcd vs. not qcd
    The categories are determined from the truth label stored in test_Y
    '''
    fig, ax = plt.subplots(1, 3, sharey=True)
    fig.set_figheight(5)
    fig.set_figwidth(13)

    higgs = [predictions[i][0] for i in range(len(predictions)) if test_Y[i][0] == 1]
    not_higgs = [predictions[i][0] for i in range(len(predictions)) if test_Y[i][0] == 0]
    ax[0].hist(higgs, density=True, bins=50, color='#259d78', alpha=.3, histtype='stepfilled', label='higgs')
    ax[0].hist(not_higgs, density=True, bins=50, color='#666666', alpha=.3, histtype='stepfilled', label='not higgs')
    ax[0].set_xlabel('RNN Higgs prediction')
    ax[0].set_ylabel('arbitrary units')
    ax[0].legend(loc='upper center')

    top = [predictions[i][1] for i in range(len(predictions)) if test_Y[i][1] == 1]
    not_top = [predictions[i][1] for i in range(len(predictions)) if test_Y[i][1] == 0]
    ax[1].hist(top, density=True, bins=50, color='#e5aa28', alpha=.3, histtype='stepfilled', label='top')
    ax[1].hist(not_top, density=True, bins=50, color='#666666', alpha=.3, histtype='stepfilled', label='not top')
    ax[1].set_xlabel('RNN Top prediction')
    ax[1].legend(loc='upper center')

    qcd = [predictions[i][2] for i in range(len(predictions)) if test_Y[i][2] == 1]
    not_qcd = [predictions[i][2] for i in range(len(predictions)) if test_Y[i][2] == 0]
    ax[2].hist(qcd, density=True, bins=50, color='#e5308a', alpha=.3, histtype='stepfilled', label='qcd')
    ax[2].hist(not_qcd, density=True, bins=50, color='#666666', alpha=.3, histtype='stepfilled', label='not qcd')
    ax[2].set_xlabel('RNN QCD prediction')
    ax[2].legend(loc='upper center')

    plt.savefig('plots/sb.png', dpi=180)
    plt.savefig('plots/sb.pdf')
    return fig, ax

def plot_rocs(predictions, test_Y):
    '''
    Plot the ROC curves for higgs efficiency vs. top rej and higgs efficiency vs. qcd rej
    '''
    stepsize = 1e-2
    cut = 1.0
    higgs_eff = []
    top_rej = []  # rejection is defined as 1 - efficiency
    qcd_rej = []
    top_sup = []  # suppression is defined as 1 / efficiency
    qcd_sup = []

    higgs_predictions = [predictions[i][0] for i in range(len(predictions)) if test_Y[i][0] == 1]
    top_predictions = [predictions[i][1] for i in range(len(predictions)) if test_Y[i][1] == 1]
    qcd_predictions = [predictions[i][2] for i in range(len(predictions)) if test_Y[i][2] == 1]

    while cut > 0:
        heff = sum([1.0 for p in higgs_predictions if p >= cut]) / float(len(higgs_predictions))
        teff = sum([1.0 for p in top_predictions if p >= cut]) / float(len(top_predictions))
        qeff = sum([1.0 for p in qcd_predictions if p >= cut]) / float(len(qcd_predictions))

        higgs_eff.append(heff)
        top_rej.append(1. - teff)
        qcd_rej.append(1. - qeff)
        try:
            top_sup.append(1. / teff)
        except ZeroDivisionError:
            top_sup.append(np.nan)
        try:
            qcd_sup.append(1. / qeff)
        except ZeroDivisionError:
            qcd_sup.append(np.nan)

        cut -= 0.01

    fig, ax = plt.subplots(2, 2, sharex=True, sharey='row')
    fig.set_figheight(10)
    fig.set_figwidth(13)

    # need to convert to numpy array for NaN masking
    higgs_eff = np.array(higgs_eff)
    top_sup = np.array(top_sup)
    qcd_sup = np.array(qcd_sup)

    ax[0,0].plot(higgs_eff, top_rej, 'o-', color='#82b2d2', markeredgewidth=0)
    ax[0,1].plot(higgs_eff, qcd_rej, 'o-', color='#f583be', markeredgewidth=0)
    ax[1,0].plot(higgs_eff[~np.isnan(top_sup)], top_sup[~np.isnan(top_sup)], 'o-', color='#82b2d2', markeredgewidth=0)
    ax[1,1].plot(higgs_eff[~np.isnan(qcd_sup)], qcd_sup[~np.isnan(qcd_sup)], 'o-', color='#f583be', markeredgewidth=0)

    ax[0,0].set_xlabel('Higgs efficiency')
    ax[0,1].set_xlabel('Higgs efficiency')
    ax[1,0].set_xlabel('Higgs efficiency')
    ax[1,1].set_xlabel('Higgs efficiency')

    ax[0,0].set_ylabel('Top rejection (1 - efficiency)')
    ax[0,1].set_ylabel('QCD rejection (1 - efficiency)')
    ax[1,0].set_ylabel('Top suppression (1 / efficiency)')
    ax[1,1].set_ylabel('QCD suppression (1 / efficiency)')

    fig.savefig('plots/rocs.png', dpi=180)
    fig.savefig('plots/rocs.pdf')


if __name__ == '__main__':
    test_X_categorylike, test_X_highlevel, test_Y = load_data()
    model = load_model()
    predictions = model.predict([test_X_categorylike, test_X_highlevel])

    plot_sb_distributions(predictions, test_Y)

    plot_rocs(predictions, test_Y)

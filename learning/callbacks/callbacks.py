'''
Some custom callbacks that plot stuff
'''

import numpy as np
import matplotlib.pyplot as plt
import time

from keras import backend as K
from keras.callbacks import Callback

class PlotLearningSimple(Callback):
    '''
    Plot loss and accuracy after every epoch
    '''

    def __init__(self, n_epochs, plotdir):
        super().__init__()
        self.epochs = n_epochs
        self.i = 0
        self.x = []
        self.logs = []
        self.plotdir = plotdir

    def on_train_begin(self, logs={}):
        self._reset()

    def _reset(self):
        '''Resets all attributes'''
        self.i = 0
        self.x = []
        self.logs = []

    def on_epoch_end(self, epoch, logs={}):
        logs['lr'] = K.get_value(self.model.optimizer.lr)
        self.logs.append(logs)
        self.x.append(self.i)
        self.i += 1
        fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        fig.set_figheight(8)
        fig.set_figwidth(13)

        x = list(range(1, len(self.logs) + 1))
        losses = [log['loss'] for log in self.logs]
        val_losses = [log['val_loss'] for log in self.logs]
        accuracies = [log['acc'] for log in self.logs]
        val_accuracies = [log['val_acc'] for log in self.logs]
        learning_rates = [log['lr'] for log in self.logs]

        fig.suptitle('RNN metrics after training {} epochs'.format(epoch))

        # ax1.set_yscale('log')
        ax1.set_xlim(-1, self.epochs + 1)
        ax1.plot(x, losses, label="loss - fat jet label",
                 c='#b51255',  marker='o')
        ax1.plot(x, val_losses, label="val_loss - fat jet label",
                 c='#965872', markerfacecolor='none', ls=':',  marker='o')
        max_y1 = np.max(np.concatenate((losses, val_losses)))
        y1lim = 1.5 * max_y1
        ax1.set_ylim(0.1, y1lim)
        ax1.legend()

        ax2.plot(x, accuracies, label="acc - fat jet label",
                 c='#146631',  marker='o')
        ax2.plot(x, val_accuracies, label="val_acc - fat jet label",
                 c='#4f9b6a', markerfacecolor='none', ls=':',  marker='o')
        max_y2 = np.max(np.concatenate((accuracies, val_accuracies)))
        y2lim = 1.5 * max_y2
        ax2.set_ylim((0, y2lim))
        ax2.legend()

        plt.xlabel('Epoch')

        # annotate learning rate changes
        # we need to create a dashed vertical line at the epoch
        # and add annotation text
        last_lr = -1
        for idx, this_lr in enumerate(learning_rates):
            if this_lr != last_lr:
                ax1.axvline(x=x[idx] - 1, ls=':', c='#9ea1a3', lw=1)
                ax2.axvline(x=x[idx] - 1, ls=':', c='#9ea1a3', lw=1)
                ax1.text(x[idx] - 1 + 0.1, 0.9 * y1lim,
                         'LR = {:1.1e}'.format(this_lr),
                         {'color': '#9ea1a3', 'fontsize': 8})
                ax2.text(x[idx] - 1 + 0.1, 0.9 * y2lim,
                         'LR = {:1.1e}'.format(this_lr),
                         {'color': '#9ea1a3', 'fontsize': 8})
                last_lr = this_lr

        # plt.show()
        t = time.strftime('%Y-%m-%d')
        name = '{}-epoch{}'.format(t, epoch)
        plt.savefig(self.plotdir + '/' + name + '.png', dpi=180)
        plt.savefig(self.plotdir + '/' + name + '.pdf')
        name = '{}-latest.pdf'.format(t)
        plt.savefig(self.plotdir + '/' + name)
        plt.clf()
        plt.close()

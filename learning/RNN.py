#!/usr/bin/env python3

'''
(c) 2018 Andreas Hoenle (der.andi@cern.ch) for the benefit of the ATLAS collaboration
'''

import sys
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'

import time
import json
import numpy as np

import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt

from keras.models import load_model
from keras.optimizers import adam
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint

from sklearn.preprocessing import OneHotEncoder

from utils import get_project_dir

sys.path.append(os.path.join(get_project_dir(), 'data_processing'))
# sys.path.append('/fatjet_rnn/data_processing')
from truthcategories import *
from jetattributes import trk_attr
from normalization import Normalizer
from dataset import Dataset

from callbacks.callbacks import PlotLearningSimple

if len(sys.argv) > 1 and sys.argv[1] == 'debug':
    # for the VSCode debugger
    import ptvsd
    print('Waiting for debugger to attach')
    ptvsd.enable_attach("my_secret", address=('0.0.0.0', 3000))
    ptvsd.wait_for_attach()
    print('Attached')

MASKING_VALUE = -999


class RNN:
    def __init__(self, cfg):
        with open(cfg) as cfgfile:
            self.cfg = json.load(cfgfile)
        for attr in ['name', 'model', 'make_plots',
                     'higgs', 'top', 'qcd', 'adam']:
            if not attr in self.cfg:
                raise ValueError('Missing attribute {} in config {}'
                                 .format(attr, cfgfile))

    def hist_compare(self, higgs, top, qcd, suffix, variable):
        '''Plot the distribution of max_tracks leading tracks of specified variable'''

        colors = ['r', 'g', 'b']
        labels = ['higgs', 'top', 'qcd']

        if variable == 'IP3D_signed_d0_significance':
            nbins = 400
            xlim = (-20, 80)
        elif variable == 'ptfrac_fj':
            nbins = 50
            xlim = (0, 0.2)
        else:
            nbins = 50
            xlim = None

        axis, _ = plt.subplots()
        for i, dataset in enumerate([higgs, top, qcd]):
            vals, weights = dataset.broadcast()
            vals = vals[:, trk_attr[variable]]
            plt.hist(vals[~np.isnan(vals)], weights=weights[~np.isnan(vals)],
                     density=True, bins=nbins, histtype='stepfilled',
                     color=colors[i], alpha=0.3, label=labels[i], linewidth=4)
        axis.legend()
        if xlim:
            plt.xlim(*xlim)
        plt.title('Comparison of {}'.format(variable))
        plt.xlabel(variable)
        plt.ylabel('aribtrary units (histograms are normed)')
        # plt.show()
        path = os.path.join(
            get_project_dir(), 'plots', 'model_training', self.cfg['name'])
        name = '{}{}'.format(variable, suffix)
        fname = os.path.join(path, name)
        plt.savefig(fname + '.pdf')
        plt.savefig(fname + '.png', dpi=180)
        plt.clf()
        plt.close()

    def shuffle(self, X, Y, weights):
        '''
        Shuffle input arrays
        '''
        np.random.seed(20160704)
        perm = np.random.permutation(len(X))
        return X[perm], Y[perm], weights[perm]

    def one_hot_encode(self, X, Y):
        enc = OneHotEncoder()
        enc.fit(Y.reshape(-1, 1))
        ohe_Y = (
            enc.transform(Y.reshape(-1, 1))
            .toarray()
            .reshape(X.shape[0], X.shape[1], -1)
        )
        # at this point ohe_Y has shape (nevents, ntracks, ncategories)
        # but after the LSTM layer the ntracks dimension is gone
        # we need shape (nevents, ncategories)
        reshape_Y = np.empty((ohe_Y.shape[0], ohe_Y.shape[2]))
        reshape_Y[:, :] = ohe_Y[:, 0, :]
        return reshape_Y

    def train(self):
        # load datasets
        higgs = Dataset(os.path.join(get_project_dir(), self.cfg['higgs']['data']),
                        max_read=self.cfg['higgs']['max_read'], truth_cut=CAT_HIGGS)
        top = Dataset(os.path.join(get_project_dir(), self.cfg['top']['data']),
                      max_read=self.cfg['top']['max_read'], truth_cut=CAT_TOP)
        qcd = Dataset(os.path.join(get_project_dir(), self.cfg['qcd']['data']),
                      max_read=self.cfg['qcd']['max_read'], truth_cut=CAT_QCD)

        # all variables that go into the training
        if 'variables' not in self.cfg:
            raise DeprecationWarning(
                'Using default dariables. This will be forbidden soon.')
            variables = [
                'numberOfInnermostPixelLayerHits',
                'numberOfNextToInnermostPixelLayerHits',
                'numberOfPixelHits',
                'numberOfPixelHoles',
                'numberOfPixelSharedHits',
                'numberOfPixelSplitHits',
                'numberOfSCTHits',
                'numberOfSCTHoles',
                'numberOfSCTSharedHits',
                'IP3D_signed_d0',
                'IP3D_signed_z0',
                'IP3D_signed_d0_significance',
                'IP3D_signed_z0_significance',
                'dr_fj',      # added by us
                'ptfrac_fj',  # added by us
                'min_deta',   # added by us
                'max_deta'    # added by us
            ]
        else:
            variables = self.cfg['variables']

        if self.cfg['make_plots']:
            print('Some plots')
            for variable in variables:
                self.hist_compare(higgs, top, qcd, '', variable)

        # normalize data with normalizer class
        normalizer = Normalizer()
        normalizer.load_from(os.path.join(
            get_project_dir(), 'data', 'normalization.json'))
        higgs.values = normalizer.normalize(higgs)
        top.values = normalizer.normalize(top)
        qcd.values = normalizer.normalize(qcd)

        if self.cfg['make_plots']:
            print('Some normalized plots')
            for variable in variables:
                self.hist_compare(higgs, top, qcd, '_normalized', variable)

        print('Preparing data')
        # TODO: Further clean up this part by creating class methods
        # prepare data
        tracks = np.concatenate((higgs.values, top.values, qcd.values), axis=0)
        weights = np.concatenate(
            (higgs.weights, top.weights, qcd.weights), axis=0)
        indices = [trk_attr[v] for v in variables]
        X = tracks[:, :, indices]
        Y = tracks[:, :, trk_attr['truth_category']]
        # replace NaN with masking value
        nan_indices = np.where(np.isnan(X))
        X[nan_indices] = MASKING_VALUE

        X, Y, weights = self.shuffle(X, Y, weights)

        # one-hot-encode Y
        Y = self.one_hot_encode(X, Y)

        print('Build and train model')
        weightpath = os.path.join(
            get_project_dir(), 'learning', 'models', self.cfg['name'],)
        weightfname = weightpath + '/weights.e{epoch:02d}-vl{val_loss:.2f}.h5'
        learning_rate_reduction = ReduceLROnPlateau(monitor='val_loss',
                                                    min_delta=0.01,
                                                    patience=5,
                                                    verbose=1,
                                                    factor=0.25,
                                                    min_lr=0.00001)
        learning_plotter = PlotLearningSimple(
            n_epochs=50,
            plotdir=os.path.join(
                get_project_dir(), 'plots', 'model_training', self.cfg['name']))
        callbacks = [EarlyStopping(verbose=True, patience=9,
                                   monitor='val_loss', min_delta=0.005),
                     ModelCheckpoint(weightfname,
                                     monitor='val_loss',
                                     verbose=True,
                                     save_best_only=False),
                     learning_plotter,
                     learning_rate_reduction]
        model = load_model(os.path.join(get_project_dir(), self.cfg['model']))
        opt = adam(lr=self.cfg['adam']['lr'])
        # model.compile(opt, loss=categorical_crossentropy_with_weights,
        #               metrics=['accuracy'])
        model.compile(opt, loss='categorical_crossentropy',
                      metrics=['accuracy'])

        if self.cfg['use_weights']:
            model.fit(X, Y, batch_size=32, sample_weight=weights[:, 0],
                      callbacks=callbacks,
                      epochs=50,
                      validation_split=0.2)
        else:
            model.fit(X, Y, batch_size=32,
                      callbacks=callbacks,
                      epochs=50,
                      validation_split=0.2)

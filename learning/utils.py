import shlex
import subprocess


def run(args, **kwargs):
    if type(args) is str:
        args = shlex.split(args)
    # may raise CalledProcessError
    result = subprocess.run(args, check=True, stdout=subprocess.PIPE, **kwargs)
    return result.stdout.decode("utf-8")


def get_project_dir() -> str:
    """Get the project directory.
    Assumptions:
        Pipenv is installed
        Pipfile is in project directory.
    Returns:
        project_dir: path to project directory
    """
    project_dir = run(["pipenv", "--where"]).strip()
    if not project_dir:
        raise RuntimeError("Could not find project directory.")
    return project_dir
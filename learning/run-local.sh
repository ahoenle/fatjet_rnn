#!/bin/bash

# jobs for running locally

trap exit SIGINT

# Note: Training logs are huge - better check callback plots for current state
# python run.py configs/2018-09-02-LSTM25-a.json # >&logs/2018-09-02-LSTM25-training.log
# python run.py configs/2018-09-02-LSTM25-b.json
# python run.py configs/2018-09-02-LSTM25-c.json
# python run.py configs/2018-09-02-LSTM25-d.json
# python run.py configs/2018-09-02-LSTM25-e.json
# python run.py configs/2018-09-02-LSTM25-f.json
# python run.py configs/2018-09-02-LSTM25-g.json
# python run.py configs/2018-09-02-LSTM25-h.json
# python run.py configs/2018-09-02-LSTM30-a.json
# python run.py configs/2018-09-02-LSTM40-a.json
# python run.py configs/2018-09-02-LSTM50-a.json
python run.py configs/2018-09-28-LSTM25-a.json
python run.py configs/2018-09-28-LSTM25-b.json
python run.py configs/2018-09-28-LSTM25-c.json

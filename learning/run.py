'''
(c) 2018 Andreas Hoenle (der.andi@cern.ch) for the benefit of the ATLAS collaboration

Train models with the supervisor.
'''

import sys
import os
import shutil
import json

from utils import get_project_dir

assert len(sys.argv) == 2, 'Usage: python run.py /path/to/config'


def make_dirs(base_dir, name):
    '''
    Prepare directories for this job
    '''
    plot_dir = os.path.join(base_dir, 'plots', 'model_training', name)
    if os.path.isdir(plot_dir):
        shutil.rmtree(plot_dir)
    os.makedirs(plot_dir)

    weights_dir = os.path.join(base_dir, 'learning', 'models', name)
    if os.path.isdir(weights_dir):
        shutil.rmtree(weights_dir)
    os.makedirs(weights_dir)



if __name__ == '__main__':
    from RNN import RNN
    cfgfile = sys.argv[1]
    with open(cfgfile) as cfgread:
        cfg = json.load(cfgread)
    make_dirs(get_project_dir(), cfg['name'])

    rnn = RNN(cfgfile)
    rnn.train()

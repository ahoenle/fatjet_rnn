'''
Creates models and saves them as h5 and json files
'''

import os
import json

from keras.layers import Input, LSTM, Dense, Dropout, Masking
from keras.models import Model

MASKING_VALUE = -999

data_dir = '/fatjet_rnn'
input_shape = (20, 17)
output_shape = 3

# LSTM25
input_layer = Input(input_shape, name='input')
x = Masking(mask_value=MASKING_VALUE)(input_layer)
x = LSTM(25, return_sequences=False, activation='relu',
            kernel_initializer='glorot_uniform')(x)
x = Dropout(0.2)(x)
y = Dense(output_shape, activation='softmax', name='output')(x)
model = Model(inputs=input_layer, outputs=y)

print(model.summary())
model.save(os.path.join(data_dir, 'learning', 'models', 'LSTM25.h5'))
with open(os.path.join(data_dir, 'learning', 'models', 'LSTM25.json'), 'w') as out:
    json.dump(model.to_json(), out)


# LSTM30
input_layer = Input(input_shape, name='input')
x = Masking(mask_value=MASKING_VALUE)(input_layer)
x = LSTM(30, return_sequences=False, activation='relu',
            kernel_initializer='glorot_uniform')(x)
x = Dropout(0.2)(x)
y = Dense(output_shape, activation='softmax', name='output')(x)
model = Model(inputs=input_layer, outputs=y)

print(model.summary())
model.save(os.path.join(data_dir, 'learning', 'models', 'LSTM30.h5'))
with open(os.path.join(data_dir, 'learning', 'models', 'LSTM30.json'), 'w') as out:
    json.dump(model.to_json(), out)

# LSTM40
input_layer = Input(input_shape, name='input')
x = Masking(mask_value=MASKING_VALUE)(input_layer)
x = LSTM(40, return_sequences=False, activation='relu',
            kernel_initializer='glorot_uniform')(x)
x = Dropout(0.2)(x)
y = Dense(output_shape, activation='softmax', name='output')(x)
model = Model(inputs=input_layer, outputs=y)

print(model.summary())
model.save(os.path.join(data_dir, 'learning', 'models', 'LSTM40.h5'))
with open(os.path.join(data_dir, 'learning', 'models', 'LSTM40.json'), 'w') as out:
    json.dump(model.to_json(), out)

# LSTM50
input_layer = Input(input_shape, name='input')
x = Masking(mask_value=MASKING_VALUE)(input_layer)
x = LSTM(50, return_sequences=False, activation='relu',
            kernel_initializer='glorot_uniform')(x)
x = Dropout(0.2)(x)
y = Dense(output_shape, activation='softmax', name='output')(x)
model = Model(inputs=input_layer, outputs=y)

print(model.summary())
model.save(os.path.join(data_dir, 'learning', 'models', 'LSTM50.h5'))
with open(os.path.join(data_dir, 'learning', 'models', 'LSTM50.json'), 'w') as out:
    json.dump(model.to_json(), out)
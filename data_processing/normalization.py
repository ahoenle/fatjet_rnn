'''
(c) 2018 Andreas Hoenle (der.andi@cern.ch) for the benefit of the ATLAS collaboration
'''

import h5py
import numpy as np
import json

from jetattributes import trk_attr
from dataset import Dataset

class Normalizer:
    '''
    A simple class that stores a mean and a stddev value.
    '''

    def __init__(self):
        self.mean = np.empty((len(trk_attr)))
        self.mean[:] = np.nan
        self.std = np.empty((len(trk_attr)))
        self.std[:] = np.nan

    def train(self, filename):
        '''
        Train the weights from the specified file
        '''
        tracks, weights = Dataset(filename).broadcast()
        # mask NaN values
        nanmask = ~np.any(np.isnan(tracks), axis=1)
        tracks = tracks[nanmask, :]
        weights = weights[nanmask]
        # calculate weighted sum
        # which is the dot product of the matrices
        weighted_sum = np.dot(tracks.T, weights)
        # divide by the sum of weights
        sum_of_weights = np.sum(weights)
        self.mean[:] = (weighted_sum / sum_of_weights).reshape(-1,)

        # weighted std is sqrt( 1/sum_of_weights * sum_i(weight * (x_i - mu)**2) )
        # calculate x_i - mu
        tmp = tracks - self.mean
        # square it
        tmp = np.square(tmp)
        # calculate weighted sum
        tmp = np.dot(tmp.T, weights)
        # divide by sum of weights
        tmp /= sum_of_weights
        # squareroot
        self.std[:] = np.sqrt(tmp).reshape(-1,)

    def to_file(self, filename):
        '''
        Store weights in filename
        '''
        data = {
            'mean': self.mean.tolist(),
            'std': self.std.tolist()
        }
        with open(filename, 'w') as f:
            json.dump(data, f)

    def load_from(self, filename):
        '''
        Load weights from filename
        '''
        with open(filename, 'r') as f:
            data = json.load(f)
        self.mean[:] = data['mean']
        self.std[:] = data['std']

    def normalize(self, dataset, inplace=True):
        '''
        Normalize dataset.values with current mean and std
        '''
        indices = [val for key, val in trk_attr.items() if key != 'truth_category']
        to_normalize = dataset.values[:, :, indices]
        normed = (to_normalize - self.mean[indices]) / self.std[indices]
        dataset.values[:, :, indices] = normed
        return dataset.values

'''
(c) 2018 Andreas Hoenle (der.andi@cern.ch) for the benefit of the ATLAS collaboration
'''

import h5py
import numpy as np

from jetattributes import trk_attr

class Dataset():
    '''
    Easyily access values and weights
    '''

    def __init__(self, fname, max_read=-1, truth_cut=None):
        h5file = h5py.File(fname, 'r')
        self._broadcasted_vals = None     # reshaped arrays for faster access when plotting
        self._broadcasted_weights = None

        # read up to max_read entries from file
        if max_read > 0:
            self.values = h5file['/tracks'][:max_read]
        else:
            self.values = h5file['/tracks'][:]

        # try to read the weights
        try:
            if max_read > 0:
                weights = h5file['/weights'][:max_read]
            else:
                weights = h5file['/weights'][:]
        except KeyError:
            print('Dataset: No weights in file {}'.format(fname))
            self.weights = None
        else:
            # reshape the weights to (nevents, ntracks)
            self.weights = np.empty(self.values.shape[:2])
            self.weights[:, :] = weights.reshape(-1, 1)

        # apply truth cut if specified
        if truth_cut is not None:
            sel = self.values[:, 0, trk_attr['truth_category']] == truth_cut
            self.values = self.values[sel]
            if self.weights is not None:
                self.weights = self.weights[sel]
            print('Dataset: Loaded dataset of length {} with truth category == {} from file {}.'
                  .format(len(self.values), truth_cut, fname))
        else:
            print('Dataset: Loaded dataset of length {} from file {}.'
                  .format(len(self.values), fname))

    def broadcast(self):
        if self._broadcasted_vals is not None:
            vals = self._broadcasted_vals
        else:
            vals = self.values.reshape(-1, self.values.shape[-1])
            self._broadcasted_vals = vals
        if self.weights is not None:
            if self._broadcasted_weights is not None:
                weights = self._broadcasted_weights
            else:
                weights = np.empty(self.values.shape[:2])
                weights[:, :] = self.weights.reshape(weights.shape)
                weights = weights.reshape(-1, 1)
                self._broadcasted_weights = weights
        return vals, weights
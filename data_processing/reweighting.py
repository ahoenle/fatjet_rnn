'''
(c) 2018 Andreas Hoenle (der.andi@cern.ch) for the benefit of the ATLAS collaboration
'''

import h5py
import numpy as np
import json

from jetattributes import fj_attr


class Reweighter:
    '''
    A class that can reweight a pt spectrum, and store the weights to a file.
    It can also load these weights and returm them to calling instances

    In its current implemenation it will always load the pt of the fatjets in the file.
    '''

    def __init__(self):
        self.bins = {}

    def train(self, filename, traintype='higgs'):
        '''
        Train the weights from the specified file
        '''
        h5file = h5py.File(filename, 'r')
        fatjets = self.flatten(h5file['/fat_jet'][:], fj_attr.keys())
        if traintype == 'higgs':
            jets = fatjets[fatjets[:, fj_attr['GhostHBosonsCount']] > 0, :]
        elif traintype == 'top':
            jets = fatjets[fatjets[:,
                                   fj_attr['GhostTQuarksFinalCount']] > 0, :]
        elif traintype == 'qcd':
            jets = fatjets[
                (fatjets[:, fj_attr['GhostHBosonsCount']] == 0) &
                (fatjets[:, fj_attr['GhostTQuarksFinalCount']] == 0), :]
        print('Found {} jets for training'.format(len(jets)))
        all_pt = jets[:, fj_attr['pt']]

        minimum = 0e7  # Start at 0.0 TeV
        maximum = 1e7  # End at 1.0 TeV
        ptlow = minimum
        pthigh = minimum
        # at most so many bins (most likely less because of tails)
        maxbins = 100
        avg_entries = int(len(all_pt) / maxbins)
        minentries = 50
        binsize = (maximum - minimum) / maxbins

        for ptlow in range(int(minimum), int(maximum), int(binsize)):
            pthigh = ptlow
            nentries = 0
            while nentries < minentries:
                pthigh += binsize
                pt_this_bin = all_pt[(all_pt > ptlow) & (all_pt < pthigh)]
                nentries = len(pt_this_bin)
                if pthigh > maximum:
                    print('train: Cannot find any more entries. '
                          'Breaking with {} entries for bin [{}, {}]'
                          .format(nentries, ptlow, pthigh))
                    break
            if nentries >= minentries:
                self.bins[ptlow] = avg_entries / float(nentries)
                print('train: Filled bin with edges [{}, {}]. #entries = {}, scalefactor = {:.2e}'.format(
                      ptlow, pthigh, nentries, self.bins[ptlow]))
            elif nentries > 0:
                self.bins[ptlow] = avg_entries / float(nentries)
                print('train: Filled low stat bin with edges [{}, {}]. #entries = {}, scalefactor = {:.2e}'.format(
                      ptlow, pthigh, nentries, self.bins[ptlow]))
                print('train: break')
                break
            else:
                # we zero out this bin because it is not supported
                self.bins[ptlow] = 0
                break

    def flatten(self, data, name_list):
        """
        Flattens a named numpy array so it can be used with pure numpy.
        Input: named array and list of names of variables in the named array
        Output: numpy array
        Example;
        print(flatten(sample_jets[['pt', 'eta']]))
        """
        ftype = [(name, float) for name in name_list]
        flat = data.astype(ftype).view(float).reshape(data.shape + (-1,))
        # return flat.swapaxes(1, len(data.shape))
        return flat

    def to_file(self, filename):
        '''
        Store weights in filename
        '''
        with open(filename, 'w') as f:
            json.dump(self.bins, f)

    def load_from(self, filename):
        '''
        Load weights from filename
        '''
        with open(filename, 'r') as f:
            bins = json.load(f)
        # print('load_from:')
        for ptlow, weight in bins.items():
            self.bins[float(ptlow)] = weight
            # print('  {}: {:.2e}'.format(ptlow, weight))

    def get_weight(self, pt):
        '''
        Return the weight for the given pt bin
        '''
        for ptlow, weight in self.bins.items():
            if ptlow > pt:
                try:
                    return last_weight
                except UnboundLocalError:
                    print(
                        'Error in get_weight: pt of {} underflows my scalefactor histogram (min supported pt is 0.3 TeV)'.format(pt))
                    return -1
            last_weight = weight
        return weight

    def __call__(self, pt):
        '''
        Make class callable - return get_weight
        '''
        return self.get_weight(pt)

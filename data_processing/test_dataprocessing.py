#!/usr/bin/env python3

'''
Simple test case to test the DataProcessor class
'''

from dataprocessing import DataProcessor

data_dir = '/Users/ahoenle/cernbox/studies/fatjet_rnn/data/'

with DataProcessor(data_dir + 'higgs/user.dguest.15192240._000001.output.h5') as p:
    p.process(outfilename='test.h5')

#!/usr/bin/env python3

'''
Load DataProcessor objects for the files that we use to train the RNN
'''

import os

from dataprocessing import DataProcessor
from reweighting import Reweighter
from normalization import Normalizer

data_dir = '/Users/ahoenle/cernbox/studies/fatjet_rnn/data/'
# data_dir = '/ptmp/hyperion/projects/fatjet_rnn/data/'


def process_sample(in_file, out_file, append=False,
                   max_events=None, reweight=False, sample_type=None):
    if sample_type is None:
        print('Error: Need to specify sample_type.')
        return
    if reweight:
        reweighter = Reweighter()
        reweighter.train(filename=in_file, traintype=sample_type)
    with DataProcessor(in_file, max_events=max_events, append=append) as p:
        if reweight:
            p.reweighter = reweighter
        p.process(outfilename=out_file, name=sample_type)


# Process Higgs samples
out_file = os.path.join(data_dir, 'higgs', 'higgs.h5')
fname = os.path.join(
    data_dir,
    'higgs',
    'user.dguest.301504.hbbTraining.e3820_s3126_s3136_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15191072._000001.output.h5')
process_sample(fname, out_file,
               append=False, max_events=-1, reweight=True, sample_type='higgs')
# normalizer.load_from(os.path.join(data_dir, 'normalization.json'))
fname = os.path.join(
    data_dir,
    'higgs',
    'user.dguest.301505.hbbTraining.e3820_s3126_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15192240._000002.output.h5')
process_sample(fname, out_file,
               append=True, max_events=-1, reweight=True, sample_type='higgs')
# Train the normalizer on this dataset (training subset)
normalizer = Normalizer()
normalizer.train(os.path.join(data_dir, 'higgs', 'higgs_train.h5'))
normalizer.to_file(os.path.join(data_dir, 'normalization.json'))

# Process top samples
out_file = os.path.join(data_dir, 'top', 'top.h5')
fname = os.path.join(
    data_dir,
    'top',
    'user.dguest.301331.hbbTraining.e4061_s3126_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15190202._000001.output.h5')
process_sample(fname, out_file,
               append=False, max_events=75e3, reweight=True, sample_type='top')
fname = os.path.join(
    data_dir,
    'top',
    'user.dguest.301331.hbbTraining.e4061_s3126_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15190202._000002.output.h5')
process_sample(fname, out_file,
               append=True, max_events=75e3, reweight=True, sample_type='top')

# Process QCD samples
out_file = os.path.join(data_dir, 'qcd', 'qcd.h5')
fname = os.path.join(
    data_dir,
    'qcd',
    'user.dguest.361025.hbbTraining.e3668_s3126_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15190671._000011.output.h5')
process_sample(fname, out_file,
               append=False, max_events=75e3, reweight=True, sample_type='qcd')
fname = os.path.join(
    data_dir,
    'qcd',
    'user.dguest.361026.hbbTraining.e3569_s3126_r9364_r9315_p3600.v1_output.h5',
    'user.dguest.15190379._000020.output.h5')
process_sample(fname, out_file,
               append=True, max_events=75e3, reweight=True, sample_type='qcd')

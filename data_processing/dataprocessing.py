'''
Writte by Andreas Hoenle <der.andi@cern.ch>

This file is based on the Re-order.ipynb notebook and wraps the functionality
in a class that can be used with python's `with` statement.
'''

import os
import h5py
import numpy as np

import matplotlib.pyplot as plt
from jetattributes import *
from truthcategories import *

# maximum number of tracks per fat jet
max_tracks = 20


class DataProcessor():
    '''
    Example usage:

        with DataProcessor('your_file.h5') as p:
            p.process()
    '''

    def __init__(self, h5_filename, append=False, max_events=-1):
        self.filename = h5_filename
        # append to the output file (default: overwrite)
        self.append = append
        self.fat_jets = None           # all fat jets from file
        self.all_vr = None             # all VR jets
        self.all_vr_tracks = None      # all tracks belonging to VR jets
        self.tracks_sorted = None      # combined tracks sorted by d0 sig
        self.reweighter = None
        if max_events is None:
            max_events = -1
        self.max_events = int(max_events)

    def __enter__(self):
        self.file = h5py.File(self.filename, 'a')
        vr1 = self.file['/subjet_VR_1'][:self.max_events].reshape(-1, 1)
        vr2 = self.file['/subjet_VR_2'][:self.max_events].reshape(-1, 1)
        vr3 = self.file['/subjet_VR_3'][:self.max_events].reshape(-1, 1)
        vr1_tracks = self.file['/subjet_VR1_tracks'][:self.max_events]
        vr2_tracks = self.file['/subjet_VR2_tracks'][:self.max_events]
        vr3_tracks = self.file['/subjet_VR3_tracks'][:self.max_events]
        self.fat_jets = self.file['/fat_jet'][:self.max_events]
        self.all_vr = np.concatenate((vr1, vr2, vr3), axis=1)
        self.all_vr_tracks = np.concatenate(
            (vr1_tracks, vr2_tracks, vr3_tracks), axis=1)
        return self

    def __exit__(self, type_par, value, traceback):
        if self.file:
            self.file.close()

    def flatten(self, data, name_list):
        """
        Flattens a named numpy array so it can be used with pure numpy.

        Input: named array and list of names of variables in the named array

        Output: numpy array

        Example;
            print(flatten(sample_jets[['pt', 'eta']]))
        """
        if name_list == trk_attr.keys():
            max_len = len(data[0][0])
            ftype = [(name, float) for name in name_list][:max_len]
            tmp = data.astype(ftype).view(float).reshape(data.shape + (-1,))
            flat = np.zeros((tmp.shape[0], tmp.shape[1], len(name_list)))
            flat[:, :, :max_len] = tmp
        else:
            ftype = [(name, float) for name in name_list]
            flat = data.astype(ftype).view(float).reshape(data.shape + (-1,))
        # return flat.swapaxes(1, len(data.shape))
        return flat

    def hist_compare_plot(self, name, higgs, higgs_weights, top, top_weights, qcd, qcd_weights, variable):
        '''Plot the distribution of max_tracks leading tracks of specified variable'''

        colors = ['r', 'g', 'b']
        labels = ['higgs', 'top', 'qcd']

        if variable == 'IP3D_signed_d0_significance':
            nb = 50
            xlim = (-20, 80)
        elif variable == 'ptfrac_fj':
            nb = 50
            xlim = (0, 0.2)
        else:
            nb = 50
            xlim = None

        plt.ticklabel_format(style='sci', scilimits=(-2, 3))
        axis, _ = plt.subplots()
        for i, (data, weights) in enumerate([(higgs, higgs_weights),
                                             (top, top_weights),
                                             (qcd, qcd_weights)]):
            plt.hist(data[~np.isnan(data)], weights=weights[~np.isnan(data)],
                     density=True, bins=nb, histtype='stepfilled',
                     color=colors[i], alpha=0.3, label=labels[i], linewidth=4)
        if xlim:
            plt.xlim(*xlim)
        axis.legend()
        plt.title('Comparison of {}'.format(variable))
        plt.xlabel(variable)
        plt.ylabel('aribtrary units (histograms are normed)')
        # plt.show()
        plt.savefig('/Users/ahoenle/cernbox/studies/fatjet_rnn/plots/preprocessing/{}_{}.png'
                    .format(name, variable),
                    dpi=180)
        plt.savefig('/Users/ahoenle/cernbox/studies/fatjet_rnn/plots/preprocessing/{}_{}.pdf'
                    .format(name, variable))
        plt.clf()
        plt.close()

    def hist_compare_trk(self, name, arr, variables, weights=None):
        # event selections
        higgs_sel = arr[:, 0, trk_attr['truth_category']] == CAT_HIGGS
        top_sel = arr[:, 0, trk_attr['truth_category']] == CAT_TOP
        qcd_sel = arr[:, 0, trk_attr['truth_category']] == CAT_QCD
        # tracks with shape (nevents, ntracks, nattr)
        higgs = arr[higgs_sel, :]
        top = arr[top_sel, :]
        qcd = arr[qcd_sel, :]
        if weights is None:
            # weights are all ones
            higgs_weights = np.ones(higgs.shape[:2]).reshape(-1,)
            top_weights = np.ones(top.shape[:2]).reshape(-1,)
            qcd_weights = np.ones(qcd.shape[:2]).reshape(-1,)
        else:
            # build weight arrays
            # shape (nevents, ntracks)
            higgs_weights = np.empty(higgs.shape[:2])
            top_weights = np.empty(top.shape[:2])
            qcd_weights = np.empty(qcd.shape[:2])
            # broadcast single weight per event to (ntrack) weights per event
            # will have shape (nevents, ntrack)
            # (nevents,) -> (nevents, 1)
            higgs_weights[:, :] = weights[higgs_sel].reshape(-1, 1)
            top_weights[:, :] = weights[top_sel].reshape(-1, 1)
            qcd_weights[:, :] = weights[qcd_sel].reshape(-1, 1)
            # reshape weights to (nevents * ntrack, 1)
            higgs_weights = higgs_weights.reshape(-1, 1)
            top_weights = top_weights.reshape(-1, 1)
            qcd_weights = qcd_weights.reshape(-1, 1)
        # reshape tracks to (nevents * ntrack, nattr)
        higgs = higgs.reshape(-1, len(trk_attr.keys()))
        top = top.reshape(-1, len(trk_attr.keys()))
        qcd = qcd.reshape(-1, len(trk_attr.keys()))
        assert len(higgs) == len(
            higgs_weights), 'data and weights have different shapes'
        for v in variables:
            self.hist_compare_plot(name + '_track',
                                   higgs[:, trk_attr[v]], higgs_weights,
                                   top[:, trk_attr[v]], top_weights,
                                   qcd[:, trk_attr[v]], qcd_weights,
                                   v)

    def hist_compare_fj(self, name, arr, variables, weights=None):
        # event selections
        higgs_sel = arr[:, fj_attr['GhostHBosonsCount']] > 0
        top_sel = arr[:, fj_attr['GhostTQuarksFinalCount']] > 0
        qcd_sel = (arr[:, fj_attr['GhostHBosonsCount']] == 0) & (
            arr[:, fj_attr['GhostTQuarksFinalCount']] == 0)
        # select fat jets
        higgs = arr[higgs_sel, :]
        top = arr[top_sel, :]
        qcd = arr[qcd_sel, :]
        if weights is None:
            higgs_weights = np.ones(len(higgs))
            top_weights = np.ones(len(top))
            qcd_weights = np.ones(len(qcd))
        else:
            higgs_weights = weights[higgs_sel]
            top_weights = weights[top_sel]
            qcd_weights = weights[qcd_sel]
        for v in variables:
            self.hist_compare_plot(name + '_fatjet',
                                   higgs[:, fj_attr[v]], higgs_weights,
                                   top[:, fj_attr[v]], top_weights,
                                   qcd[:, fj_attr[v]], qcd_weights,
                                   v)

    def process(self, outfilename, name, test_split=0.1):
        '''
        This method does the heavy lifting.
        For more details take a look in the Re-order.ipynb notebook.
        '''
        print('Starting dataprocessing for {}'.format(self.filename))
        n_fatjets = len(self.fat_jets)
        print('Selected {} events'.format(n_fatjets))

        print('Flatten fat jets')
        fj_flat = self.flatten(self.fat_jets, fj_attr.keys())
        print('Flatten vr jets')
        vr_flat = self.flatten(self.all_vr, vr_attr.keys())
        print('Flatten tracks')
        trk_flat = self.flatten(self.all_vr_tracks, trk_attr.keys())
        # make cuts on IP3D_signed_d0 and IP3D_signed_z0
        print('Cuts on IP3D signed d0/z0')
        trk_flat[abs(trk_flat[:, :, trk_attr['IP3D_signed_d0']]) > 3] = np.nan
        trk_flat[abs(trk_flat[:, :, trk_attr['IP3D_signed_z0']]) > 3] = np.nan
        # calculate additional variables
        print('Storing fatjet information')
        trk_flat[:, :, trk_attr['pt_fj']] = fj_flat[: ,fj_attr['pt']].reshape(-1, 1)
        trk_flat[:, :, trk_attr['mass_fj']] = fj_flat[:, fj_attr['mass']].reshape(-1, 1)
        print('Calc ptfrac_fj')
        trk_flat[:, :, trk_attr['ptfrac_fj']] = trk_flat[:,
                                                         :, trk_attr['pt']] / fj_flat[:, fj_attr['pt']].reshape(-1, 1)
        # cut on pt frac
        print('Cut on ptfrac_fj')
        trk_flat[trk_flat[:, :, trk_attr['ptfrac_fj']] > 0.2] = np.nan
        # deta fj
        print('Calc deta_fj')
        trk_flat[:, :, trk_attr['deta_fj']] = trk_flat[:,
                                                       :, trk_attr['eta']] - fj_flat[:, fj_attr['eta']].reshape(-1, 1)
        # dphi fj
        print('Calc dphi_fj')
        trk_flat[:, :20, trk_attr['dphi_fj']] = vr_flat[:, 0,
                                                        vr_attr['dphi']].reshape(-1, 1) + trk_flat[:, :20, trk_attr['dphi']]
        trk_flat[:, 20:40, trk_attr['dphi_fj']] = vr_flat[:, 1,
                                                          vr_attr['dphi']].reshape(-1, 1) + trk_flat[:, 20:40, trk_attr['dphi']]
        trk_flat[:, 40:, trk_attr['dphi_fj']] = vr_flat[:, 2,
                                                        vr_attr['dphi']].reshape(-1, 1) + trk_flat[:, 40:, trk_attr['dphi']]
        # dr fj
        print('Calc dr_fj')
        trk_flat[:, :, trk_attr['dr_fj']] = np.sqrt(np.square(
            trk_flat[:, :, trk_attr['deta_fj']]) + np.square(trk_flat[:, :, trk_attr['dphi_fj']]))
        print('Sort')
        sorted_tracks = np.empty(trk_flat.shape)
        for i in range(n_fatjets):
            # TODO: Vectorize?
            tracks = trk_flat[i, :, :]
            sort = tracks[np.argsort(
                np.abs(tracks[:, trk_attr['IP3D_signed_d0_significance']]))[::-1]]
            n_nan = np.count_nonzero(
                np.isnan(sort[:, trk_attr['IP3D_signed_d0_significance']]))
            sorted_tracks[i] = np.roll(sort, -n_nan, axis=0)
        print('Truncate to to max_tracks = {} tracks'.format(max_tracks))
        truncated_tracks = sorted_tracks[:, :max_tracks, :]
        print('Calc deta_min / deta_max')
        deta_arr = np.empty((sorted_tracks.shape[0], max_tracks, max_tracks))
        for i in range(max_tracks):
            for j in range(max_tracks):
                if i == j:
                    deta_arr[:, i, j] = np.nan
                    continue
                deta_arr[:, i, j] = np.abs(truncated_tracks[:, i, trk_attr['eta']
                                                            ] - truncated_tracks[:, j, trk_attr['eta']])
        truncated_tracks[:, :, trk_attr['min_deta']
                         ] = np.nanmin(deta_arr[:, :, :], axis=2)
        truncated_tracks[:, :, trk_attr['max_deta']
                         ] = np.nanmax(deta_arr[:, :, :], axis=2)

        print('Adding truth category')
        truncated_tracks[:, :, trk_attr['truth_category']
                         ] = CAT_QCD  # default category
        truncated_tracks[fj_flat[:, fj_attr['GhostTQuarksFinalCount']]
                         > 0, :, trk_attr['truth_category']] = CAT_TOP
        truncated_tracks[fj_flat[:, fj_attr['GhostHBosonsCount']]
                         > 0, :, trk_attr['truth_category']] = CAT_HIGGS

        # cut on fat jet pt
        PT_CUT = 2e6  # 2 TeV
        truncated_tracks = truncated_tracks[truncated_tracks[:,
                                                             0, trk_attr['mass_fj']] < PT_CUT]

        print('Producing some plots')
        self.hist_compare_fj(name, fj_flat, ['pt'])
        self.hist_compare_trk(name, truncated_tracks,
                              ['IP3D_signed_d0_significance', 'eta', 'deta_fj',
                               'dphi_fj', 'dr_fj', 'ptfrac_fj', 'min_deta', 'max_deta'])

        weights = None
        if self.reweighter:
            print('Calculating weights')
            # calculate weight for every event and save it in weights array
            weights = np.empty((truncated_tracks.shape[0],))
            keys = list(self.reweighter.bins.keys())
            for i, bin_lo in enumerate(keys):
                try:
                    bin_hi = keys[i + 1]
                except IndexError:
                    # last bin
                    bin_hi = 1e10
                weight = self.reweighter.bins[bin_lo]
                sel = (fj_flat[:, fj_attr['pt']] >= bin_lo) & (
                    fj_flat[:, fj_attr['pt']] < bin_hi)
                weights[sel] = weight

            print('Producing some post re-weight plots')
            self.hist_compare_fj(name + '_reweight',
                                 fj_flat, ['pt'], weights=weights)
            self.hist_compare_trk(name + '_reweight', truncated_tracks,
                                  ['IP3D_signed_d0_significance', 'eta', 'deta_fj',
                                   'dphi_fj', 'dr_fj', 'ptfrac_fj', 'min_deta', 'max_deta'],
                                  weights=weights)

        print('Saving data as h5file')
        split_idx = int((1 - test_split) * len(truncated_tracks))
        if self.append:
            with h5py.File(outfilename.replace('.h5', '_train.h5'), 'a') as f:
                f['tracks'].resize((f['tracks'].shape[0] + split_idx), axis=0)
                f['tracks'][-split_idx:] = truncated_tracks[:split_idx]
                if weights is not None:
                    f['weights'].resize(
                        (f['weights'].shape[0] + split_idx), axis=0)
                    f['weights'][-split_idx:] = weights[:split_idx]
            with h5py.File(outfilename.replace('.h5', '_test.h5'), 'a') as f:
                inv_split_idx = len(truncated_tracks) - split_idx
                f['tracks'].resize(
                    (f['tracks'].shape[0] + inv_split_idx), axis=0)
                f['tracks'][-inv_split_idx:] = truncated_tracks[split_idx:]
                if weights is not None:
                    f['weights'].resize(
                        (f['weights'].shape[0] + inv_split_idx), axis=0)
                    f['weights'][-inv_split_idx:] = weights[split_idx:]

        else:
            with h5py.File(outfilename.replace('.h5', '_train.h5'), 'w') as f:
                f.create_dataset('tracks', data=truncated_tracks[:split_idx], maxshape=(
                    None, max_tracks, len(trk_attr.keys())), chunks=True)
                if weights is not None:
                    f.create_dataset(
                        'weights', data=weights[:split_idx], maxshape=(None,), chunks=True)
            with h5py.File(outfilename.replace('.h5', '_test.h5'), 'w') as f:
                f.create_dataset('tracks', data=truncated_tracks[split_idx:], maxshape=(
                    None, max_tracks, len(trk_attr.keys())), chunks=True)
                if weights is not None:
                    f.create_dataset(
                        'weights', data=weights[split_idx:], maxshape=(None,), chunks=True)

        print('Done.')

#!/usr/bin/env python3

'''
Load DataProcessor objects for the files that we use to train the RNN
'''

from dataprocessing import DataProcessor
from reweighting import Reweighter
from normalization import Normalizer

data_dir = '/Users/ahoenle/cernbox/studies/fatjet_rnn/data/'

# train the reweighter of the higgs data
reweighter = Reweighter()
reweighter.train(data_dir + 'higgs/user.dguest.15192240._000001.output.h5')
reweighter.to_file(data_dir + '/reweighting_weights.json')
# reweighter.load_from(data_dir + '/reweighting_weights.json')

higgs_out = data_dir + '/higgs/higgs.h5'
with DataProcessor(data_dir + 'higgs/user.dguest.15192240._000001.output.h5') as p:
    p.reweighter = reweighter
    p.process(outfilename=higgs_out, name='higgs_1')
with DataProcessor(data_dir + 'higgs/user.dguest.15192240._000002.output.h5', append=True) as p:
    p.reweighter = reweighter
    p.process(outfilename=higgs_out, name='higgs_2')

with DataProcessor(data_dir + 'top/user.dguest.301328.hbbTraining.e4061_s3126_r9364_r9315_p3600.v1_output.h5/user.dguest.15190578._000001.output.h5') as p:
    p.reweighter = reweighter
    p.process(outfilename=data_dir + 'top/top.h5', name='top')

with DataProcessor(data_dir + 'qcd/user.dguest.361024.hbbTraining.e3668_s3126_r9364_r9315_p3600.v1_output.h5/user.dguest.15190376._000046.output.h5') as p:
    p.reweighter = reweighter
    p.process(outfilename=data_dir + 'qcd/qcd.h5', name='qcd')

# train the normalizer on these sets
normalizer = Normalizer()
normalizer.train(data_dir + '/higgs/higgs_train.h5')
normalizer.to_file(data_dir + '/normalization.json')
# normalizer.load_from(data_dir + '/normalization.json')

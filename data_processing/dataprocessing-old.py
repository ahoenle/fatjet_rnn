'''
Writte by Andreas Hoenle <der.andi@cern.ch>

This file is based on the Re-order.ipynb notebook and wraps the functionality
in a class that can be used with python's `with` statement.
'''

from multiprocessing import Pool, current_process
import os
import h5py
import numpy as np
import pandas as pd

# maximum number of tracks per fat jet
max_tracks = 20

# event categories
CAT_HIGGS, CAT_TOP, CAT_QCD = range(3)

# jet attributes
trk_attr = {
    'chiSquared': 0,
    'numberDoF': 1,
    'IP3D_signed_d0': 2,
    'IP2D_signed_d0': 3,
    'IP3D_signed_z0': 4,
    'IP3D_signed_d0_significance': 5,
    'IP3D_signed_z0_significance': 6,
    'numberOfInnermostPixelLayerHits': 7,
    'numberOfNextToInnermostPixelLayerHits': 8,
    'numberOfPixelHits': 9,
    'numberOfPixelHoles': 10,
    'numberOfPixelSharedHits': 11,
    'numberOfPixelSplitHits': 12,
    'numberOfSCTHits': 13,
    'numberOfSCTHoles': 14,
    'numberOfSCTSharedHits': 15,
    'pt': 16,
    'eta': 17,
    'deta': 18,
    'dphi': 19,
    'dr': 20,
    'ptfrac': 21,
    'deta_fj': 22,           # added by us
    'dphi_fj': 23,           # added by us
    'dr_fj': 24,             # added by us
    'ptfrac_fj': 25,         # added by us
    'truth_category': 26,    # added by us
}
vr_attr = {
    'MV2c10_discriminant': 0,
    'MV2c10mu_discriminant': 1,
    'MV2c10rnn_discriminant': 2,
    'DL1_pu': 3,
    'DL1_pc': 4,
    'DL1_pb': 5,
    'DL1rnn_pu': 6,
    'DL1rnn_pc': 7,
    'DL1rnn_pb': 8,
    'IP2D_pu': 9,
    'IP2D_pc': 10,
    'IP2D_pb': 11,
    'IP3D_pu': 12,
    'IP3D_pc': 13,
    'IP3D_pb': 14,
    'SV1_pu': 15,
    'SV1_pc': 16,
    'SV1_pb': 17,
    'rnnip_pu': 18,
    'rnnip_pc': 19,
    'rnnip_pb': 20,
    'rnnip_ptau': 21,
    'JetFitter_energyFraction': 22,
    'JetFitter_mass': 23,
    'JetFitter_significance3d': 24,
    'JetFitter_deltaphi': 25,
    'JetFitter_deltaeta': 26,
    'JetFitter_massUncorr': 27,
    'JetFitter_dRFlightDir': 28,
    'SV1_masssvx': 29,
    'SV1_efracsvx': 30,
    'SV1_significance3d': 31,
    'SV1_dstToMatLay': 32,
    'SV1_deltaR': 33,
    'SV1_Lxy': 34,
    'SV1_L3d': 35,
    'JetFitter_nVTX': 36,
    'JetFitter_nSingleTracks': 37,
    'JetFitter_nTracksAtVtx': 38,
    'JetFitter_N2Tpair': 39,
    'SV1_N2Tpair': 40,
    'SV1_NGTinSvx': 41,
    'GhostBHadronsFinalCount': 42,
    'GhostCHadronsFinalCount': 43,
    'pt': 44,
    'eta': 45,
    'deta': 46,
    'dphi': 47,
    'dr': 48
}
fj_attr = {
    'Split12': 0,
    'Split23': 1,
    'Qw': 2,
    'PlanarFlow': 3,
    'Angularity': 4,
    'Aplanarity': 5,
    'ZCut12': 6,
    'KtDR': 7,
    'HbbScore': 8,
    'XbbScoreQCD': 9,
    'XbbScoreTop': 10,
    'XbbScoreHiggs': 11,
    'pt': 12,
    'eta': 13,
    'GhostHBosonsCount': 14,
    'GhostWBosonsCount': 15,
    'GhostZBosonsCount': 16,
    'GhostTQuarksFinalCount': 17,
    'mcEventWeight': 18,
    'eventNumber': 19,
    'mass': 20,
    'C2': 21,
    'D2': 22,
    'e3': 23,
    'Tau21_wta': 24,
    'Tau32_wta': 25,
    'FoxWolfram20': 26
}


class DataProcessor():
    '''
    Usage: For example
    with DataProcessor('your_file.h5') as p:
        p.process()
    '''

    def __init__(self, h5_filename):
        self.filename = h5_filename
        self.fat_jets = None           # all fat jets from file
        self.all_vr = None             # all VR jets
        self.all_vr_tracks = None      # all tracks belonging to VR jets
        self.tracks_sorted = None      # combined tracks sorted by d0 sig

    def __enter__(self):
        self.file = h5py.File(self.filename, 'a')
        vr1 = self.file['/subjet_VR_1'][:].reshape(-1, 1)
        vr2 = self.file['/subjet_VR_2'][:].reshape(-1, 1)
        vr3 = self.file['/subjet_VR_3'][:].reshape(-1, 1)
        vr1_tracks = self.file['/subjet_VR1_tracks'][:]
        vr2_tracks = self.file['/subjet_VR2_tracks'][:]
        vr3_tracks = self.file['/subjet_VR3_tracks'][:]
        self.fat_jets = self.file['/fat_jet'][:]
        self.all_vr = np.concatenate((vr1, vr2, vr3), axis=1)
        self.all_vr_tracks = np.concatenate(
            (vr1_tracks, vr2_tracks, vr3_tracks), axis=1)
        return self

    def __exit__(self, type_par, value, traceback):
        if self.file:
            self.file.close()

    def multiprocess(self, function, max_index, nprocesses=8):
        '''
        Execute the given function up to the max_index with nprocesses
        '''
        per_job = max_index // nprocesses
        idx_low = 0
        idx_high = 0
        args = []
        while idx_high < max_index:
            idx_high += per_job
            if max_index - idx_high < per_job:
                # it's not worth creating an additional thread for the rest
                idx_high = max_index
            args.append((idx_low, idx_high))
            idx_low += per_job

        print('multiprocess: prepared function arguments: {}'.format(args))
        print('multiprocess: Generating pool')
        pool = Pool(nprocesses)
        print('multiprocess: Starting workers')
        results = pool.map(function, args)
        print('multiproess: pool.map is done')
        concat = np.concatenate(results, axis=0)
        return concat

    def fill_part_and_sort(self, *args):
        '''
        This method is called by the threads that are created to fill
        the nparray with the elements from all_vr_tracks

        The args are expected to be the lower and the upper index of the for
        loop.
        '''
        # get indices from args
        idx_low = args[0][0]
        idx_high = args[0][1]
        print('fill_part_and_sort: from {} to {} - {}'.format(idx_low,
                                                              idx_high, current_process()))
        # initialize empty arrays
        unsorted_tracks = np.empty((idx_high - idx_low, 60, len(trk_attr.keys())))
        sorted_tracks = np.empty((idx_high - idx_low, 60, len(trk_attr.keys())))
        for i in range(idx_low, idx_high):
            # fill the tracks and calculate additional quantities
            all_tracks = self.all_vr_tracks[i]
            # calculate the truth label
            fatjet = self.fat_jets[i]
            if fatjet[fj_attr['GhostHBosonsCount']] > 0:
                category = CAT_HIGGS
            elif fatjet[fj_attr['GhostTQuarksFinalCount']] > 0:
                category = CAT_TOP
            else:
                category = CAT_QCD
            for j in range(60):
                # if the track does not pass the cuts we fill only NaNs
                if (abs(all_tracks[j][trk_attr['IP3D_signed_d0']]) > 3
                        or abs(all_tracks[j][trk_attr['IP3D_signed_z0']]) > 3):
                    unsorted_tracks[i - idx_low, j, :] = np.nan
                    continue
                # if we arrive here the track has passed the cuts
                # each track has 22 features
                for k in range(22):
                    unsorted_tracks[i - idx_low, j, k] = all_tracks[j][k]
                # calculate additional features like the dphi to the VR jet
                # deta to the fat jet
                unsorted_tracks[i - idx_low, j, trk_attr['deta_fj']] = (
                    unsorted_tracks[i - idx_low, j, trk_attr['eta']] - fatjet[fj_attr['eta']]
                )
                # dphi to the fat jet
                vrjet = self.all_vr[i, j // 20]
                unsorted_tracks[i - idx_low, j, trk_attr['dphi_fj']] = (
                    vrjet[vr_attr['dphi']] + all_tracks[j][trk_attr['dphi']])
                # dr to the fat jet
                unsorted_tracks[i - idx_low, j, trk_attr['dr_fj']] = (
                    np.sqrt(
                        np.power(unsorted_tracks[i - idx_low, j, trk_attr['deta_fj']], 2) +
                        np.power(unsorted_tracks[i - idx_low, j, trk_attr['dphi_fj']], 2)
                    )
                )
                # pt fraction of the fat jet
                unsorted_tracks[i - idx_low, j, trk_attr['ptfrac_fj']] = (
                    all_tracks[j][trk_attr['pt']] / self.fat_jets[i][fj_attr['pt']])
                # add a cut on the pt fraction
                if unsorted_tracks[i - idx_low, j, trk_attr['ptfrac_fj']] > 0.2:
                    unsorted_tracks[i - idx_low, j, :] = np.nan
                    continue
                # add the truth label
                unsorted_tracks[i - idx_low, j, trk_attr['truth_category']] = category
            # sort the tracks (brings NaN to the top)
            tracks = unsorted_tracks[i - idx_low]
            sort = tracks[np.argsort(
                np.abs(tracks[:, trk_attr['IP3D_signed_d0_significance']]))[::-1]]
            # count number of NaNs and roll them to the back
            n_nan = np.count_nonzero(
                np.isnan(sort[:, trk_attr['IP3D_signed_d0_significance']]))
            sorted_tracks[i - idx_low] = np.roll(sort, -n_nan, axis=0)
        print('fill_part_and_sort: from {} to {} done'.format(idx_low, idx_high))
        return sorted_tracks[:, :max_tracks, :]

    def process(self, outfilename):
        '''
        This method does the heavy lifting.
        For more details take a look in the Re-order.ipynb notebook.
        '''
        n_fatjets = len(self.fat_jets)
        print('This file has {} events'.format(n_fatjets))

        # because the h5py file is not pickable we have to close it here
        self.file.close()
        self.file = None
        print('Filling numpy array of correct shape')
        self.tracks_sorted = self.multiprocess(self.fill_part_and_sort,
                                               max_index=n_fatjets, nprocesses=8)
        assert self.tracks_sorted.shape == (n_fatjets, max_tracks, len(trk_attr.keys()))
        # now after the multiprocessing is done we can open the file again
        self.file = h5py.File(self.filename, 'a')

        print('Converting to pandas dataframe')
        data = pd.DataFrame()
        for track_idx in range(max_tracks):
            for attr_name, attr_idx in trk_attr.items():
                data[attr_name + '_trk' + str(track_idx)] = self.tracks_sorted[:,
                                                                               track_idx, attr_idx]

        print('Printing the d0 significances of event 25.')
        print('!! -> Make sure that this is sorted. Otherwise there is a bug <- !!')
        for i in range(max_tracks):
            print(data['IP3D_signed_d0_significance_trk' + str(i)][25])

        print('Calculating min/max deta')
        for i in range(max_tracks):
            deta = pd.DataFrame()
            # we could loop j from i+1 to 59
            # but that would only give the correct minimum for the track(i,j) distance, not for the
            # track(j,i) distance instead of finding the minimum j index and saving the min distance
            # also there it's easier for me to just run j from 0 to 59 too
            for j in range(max_tracks):
                if j == i:
                    continue
                deta[str(i) + ',' + str(j)] = abs(data['eta_trk' +
                                                       str(i)] - data['eta_trk' + str(j)])
            data['min_deta_trk' + str(i)] = deta.min(axis=1, skipna=True)
            data['max_deta_trk' + str(i)] = deta.max(axis=1, skipna=True)

        # NB: It is not very elegant to first store the truth label for every event and then throw
        #     away most of the labels. However, it was the easiest solution to begin with
        print('Dropping duplicate truth_category entries')
        data['truth_category'] = data['truth_category_trk0']
        truth_per_track_columns = ['truth_category_trk' + str(i) for i in range(max_tracks)]
        data.drop(truth_per_track_columns, axis=1, inplace=True)

        # dataset = f.create_dataset('all_tracks', data=data.values)
        print('Writing outfile')
        if os.path.isfile(outfilename):
            print('Removing existing outfile: {}'.format(outfilename))
            os.remove(outfilename)
        data.to_hdf(outfilename, 'data')
        print('Done.')
